{-# LANGUAGE OverloadedStrings #-}

module Config where

import Data.ByteString (ByteString)
import qualified Data.Map as Map
import qualified Data.Yaml as Y
import Data.Yaml (FromJSON(..), (.:))

type PatternMap = Map.Map String String

newtype Config = Config
  { files :: [PatternMap]
  } deriving (Eq, Show)

instance FromJSON Config where
  parseJSON (Y.Object v) = Config <$> v .: "files"
  parseJSON _ = fail "Expected Object for Config value"

parseYaml :: ByteString -> Maybe Config
parseYaml a = Y.decode a :: Maybe Config
