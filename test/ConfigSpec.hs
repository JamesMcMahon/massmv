{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module ConfigSpec where

import Config
import Data.ByteString (ByteString)
import qualified Data.Map as Map
import Test.Hspec
import Text.RawString.QQ

main :: IO ()
main = hspec spec

spec :: Spec
spec =
  describe "parseYaml" $ do
    it "returns expected config object" $ do
      let (Just actual) =
            parseYaml
              [r|
                files:
                - foo: bar
                |]
      files actual `shouldBe` [Map.fromList [("foo", "bar")]]
    it "returns Nothing if data is wrong" $ do
      let actual =
            parseYaml
              [r|
                files:
                - apples
                |]
      actual `shouldBe` Nothing
