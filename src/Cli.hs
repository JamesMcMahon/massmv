{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

module Cli where

import Control.Monad.IO.Class (MonadIO(..))
import Options.Generic

data CliOptions = CliOptions
  { config :: String <?> "Documentation for the config flag"
  , verbose :: Bool
  , dryRun :: Bool
  } deriving (Generic, Show)

instance ParseRecord CliOptions

cli :: IO ()
cli = do
  x <- getRecord "Test program"
  print (x :: CliOptions)

-- cli :: (MonadIO io, ParseRecord a) -> io a
-- cli = getRecord "massmv"
