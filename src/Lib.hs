{-# LANGUAGE OverloadedStrings #-}

module Lib
  ( someFunc
  ) where

import Config
import qualified Data.ByteString as ByteString

someFunc :: IO ()
someFunc = do
  contents <- ByteString.readFile "test.yml"
  let config = parseYaml contents
  case config of
    Just config -> print config
    Nothing -> putStrLn "Fail"
