# massmv

# Use Case

- Be able to scan through all files in a directory and move files with "(u)" in them to the USA folder, "(e)" to european, etc.
- Split files into different alphabetical buckets
- put files with the same prefix (game name) into a single directory. (rather then having a bunch of betas all over the place)

put files into directories based on the result of a regex

U/Superman (U) [o1].nes
U/Teenage Mutant Ninja Turtles III - The Manhattan Project (U) [b3].nes
J/Tamura Koushou Mahjong Seminar (J) [b1].nes
Hack/Tecmo Super Bowl 90-91 By Bgiadam (Hack).nes
E/Ultimate Air Combat (E) [!].nes

# Plan

- Two programs
    - 1 to put file in a directory based on regex
    - 1 to create alphabetical buckets

# Buckets

### Possible Options

- Number of buckets
- Dynamic buckets (best fit)
- max files per directory

### Questions

- How to handle non alpha characters?
- How to handle non alpha numeric characters?


# Regmv

### Options

- pass in a regex, to pull name of directory / file
- pattern of name of new directory / file
    + '{1}/{originalfilename}'
    + pull numbers out of regex, need a token for original name
- Can probably do this simply in a shell script by chain sed into mkdir and then into mv

# Rename experiments


    [~/t/rename-test]─> rename -v -n 's:.*\((.*)\).+:$1/$_:g' *.nes
    Using expression: sub { use feature ':5.18'; s:.*\((.*)\).+:$1/$_:g }
    'River City Ransom (U) [b3].nes' would be renamed to 'U/River City Ransom (U) [b3].nes'
    'Zelda II - The Adventure of Link (E) (PRG0) [b1].nes' would be renamed to 'PRG0/Zelda II - The Adventure of Link (E) (PRG0) [b1].nes'

.*\((.*)\).+

Fix with lazy
.*?\((.*?)\).+


# massmv OR mvall OR mva

Take in a list of files (glob syntax) and a yaml file

Make the keys in the YAML to the current files, and then rename them to the value. Support regex syntax.

So

> ---
> foo/: bar/
> someGuy.txt: newGuy.txt
> bestcode.java: code/worst.java
> someRegex/: "$1/"

directory foo/ becomes bar/
file someGuy.txt becomes newGuy.txt
file bestcode.java get moved into code/ and gets renamed worst.java
someRegex gets matched on directories and the first capture group is the name of the new directory

Allow arrays of file objects to support multiple passes

> ---
> files:
> - foo: bar
>   bash: zsh
> - bar: gaz
>   zsh: fish

first pass:
    foo -> bar
    bash -> zsh
second pass:
    bar -> gaz
    zsh -> fish
